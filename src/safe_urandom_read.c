#include<stdio.h>
#include<errno.h>
#include<fcntl.h>
#include<sys/ioctl.h>
#include<sys/stat.h>
#include<unistd.h>

int random_bytes_safer(void *buf, size_t len)
{
    struct stat st;
    size_t i;
    int fd, cnt, flags;
    int save_errno = errno;

start:
    flags = O_RDONLY;
#ifdef O_NOFOLLOW
    flags |= O_NOFOLLOW;
#endif
#ifdef O_CLOEXEC
    flags |= O_CLOEXEC;
#endif

    fd = open("/dev/urandom", flags, 0);
    if(fd == -1)
    {
        if(errno == EINTR)
        {
            goto start;
        }
        goto nodevrandom;
    }
#ifndef O_CLOEXEC
    fcntl(fd, F_SETFD, fcnctl(fd, F_GETFD) | FD_CLOEXEC);
#endif

    if((fstat(fd, &st) == -1) || !S_ISCHR(st.st_mode))
    {
        close(fd);
        goto nodevrandom;
    }

    if(ioctl(fd, RNDGETENTCNT, &cnt) == -1)
    {
        close(fd);
        goto nodevrandom;
    }
    for(i = 0; i < len;)
    {
        size_t wanted = len - i;
        ssize_t ret = read(fd, (char *)buf + i, wanted);
        if(ret == -1)
        {
            if(errno == EAGAIN || errno == EINTR)
            {
                continue;
            }
            close(fd);
            goto nodevrandom;
        }
        i += ret;
    }
    close(fd);
    if(gotdata(buf, len) == 0)
    {
        errno = save_errno;
        return 0;
    }
nodevrandom:
    errno = EIO;
    return -1;
}


int main()
{

}
